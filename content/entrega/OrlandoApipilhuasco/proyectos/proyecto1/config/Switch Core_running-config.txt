!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
 switchport access vlan 10
 switchport mode trunk
!
interface GigabitEthernet1/1
 switchport access vlan 20
 switchport mode trunk
!
interface GigabitEthernet2/1
 switchport access vlan 30
 switchport mode trunk
!
interface GigabitEthernet3/1
 switchport mode trunk
!
interface FastEthernet4/1
!
interface FastEthernet5/1
!
interface FastEthernet6/1
!
interface FastEthernet7/1
!
interface FastEthernet8/1
!
interface FastEthernet9/1
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan10
 mac-address 0030.a3da.6b01
 ip address 192.168.10.4 255.255.255.0
 ip helper-address 192.168.2.1
 ip helper-address 192.168.10.4
!
interface Vlan20
 mac-address 0030.a3da.6b02
 ip address 192.168.11.4 255.255.255.0
 ip helper-address 192.168.3.1
 ip helper-address 192.168.11.4
!
interface Vlan30
 mac-address 0030.a3da.6b03
 ip address 192.168.12.4 255.255.255.0
 ip helper-address 192.168.4.1
 ip helper-address 192.168.12.4
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

